# Voltage

[![build status](https://gitlab.com/voltage/voltage/badges/master/build.svg)](https://gitlab.com/voltage/voltage/commits/master)

Voltage, the Volleyball Tournament Manager, is a web application
for planning and supporting volleyball tournaments. It is
licensed under the conditions of the MIT license
(cf. LICENSE for details).

### Development

Voltage is a web application based
on the [Django](https://www.djangoproject.com/) Framework.
Its central Git repository resides at
[GitLab](https://gitlab.com/voltage/voltage).
