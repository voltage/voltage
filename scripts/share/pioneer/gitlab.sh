#!/bin/sh
#
# Copyright (c) 2016 The Voltage Project
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# set up the environment
get_os() {
    [ -x "/usr/bin/uname" ] && b_uname="/usr/bin/uname" || b_uname="/bin/uname"
    ${b_uname} -s
}

real_path() {
    local os=$(get_os)
    if [ "${os}" = "Darwin" ]; then
        [ -x "/usr/local/bin/perl" ] && b_perl="/usr/local/bin/perl" || b_perl="/usr/bin/perl"
        ${b_perl} -MCwd -le 'print Cwd::abs_path(shift)' "$0"
    else
        [ -x "/bin/realpath" ] && b_realpath="/bin/realpath" || b_realpath="/usr/bin/realpath"
        ${b_realpath} "$0"
    fi
}

SCRIPTPATH=$(real_path $0)
SCRIPTPREFIX=${SCRIPTPATH%/*}
. ${SCRIPTPREFIX}/common.sh

b_apt=$(detect_binary "apt-get")
[ ! -x "${b_apt}" ] && die 1 "failed to detect working apt-get installation"

progress_msg "Performing baseline update"
{
    ${b_apt} -y update
} >/dev/null 2>&1
rval=$?
[ "${rval}" -ne "0" ] && progress_fail && die 1 "apt-get -y update failed"
[ "${rval}" -eq "0" ] && progress_success

progress_msg "Installing OS package dependencies"
{
    ${b_apt} -y install python3-pip
} >/dev/null 2>&1
rval=$?
[ "${rval}" -ne "0" ] && progress_fail && die 1 "apt-get -y install python3-pip failed"
{
    ${b_apt} -y install python3-dev python3-setuptools
} >/dev/null 2>&1
rval=$?
[ "${rval}" -ne "0" ] && progress_fail && die 1 "apt-get -y install python3-dev python3-setuptools failed"
{
    ${b_apt} -y install git
} >/dev/null 2>&1
rval=$?
[ "${rval}" -ne "0" ] && progress_fail && die 1 "apt-get -y install git failed"
{
    ${b_apt} -y install supervisor
} >/dev/null 2>&1
rval=$?
[ "${rval}" -ne "0" ] && progress_fail && die 1 "apt-get -y install supervisor failed"
[ "${rval}" -eq "0" ] && progress_success

b_pip=$(detect_binary "pip")
[ ! -x "${b_pip}" ] && die 1 "failed to detect working pip installation"

progress_msg "Installing Python package dependencies"
{
    ${b_pip} install --upgrade pip
} >/dev/null 2>&1
rval=$?
[ "${rval}" -ne "0" ] && progress_fail && die 1 "pip install --upgrade pip failed"
{
    ${b_pip} install -r requirements.txt
} >/dev/null 2>&1
rval=$?
[ "${rval}" -ne "0" ] && progress_fail && die 1 "pip install -r requirements.txt failed"
[ "${rval}" -eq "0" ] && progress_success

package_list="$(${b_pip} freeze)"
if [ -n "${package_list}" ]; then
    b_awk=$(detect_binary "awk")
    log_info "List of installed Python packages:"
    for package in ${package_list}; do
        package_name=$(echo ${package} | ${b_awk} 'BEGIN { FS = "==" } { print $1 }')
        package_version=$(echo ${package} | ${b_awk} 'BEGIN { FS = "==" } { print $2 }')
        log_info "  ${package_name} (${package_version})"
    done
fi

exit ${STATUS}
