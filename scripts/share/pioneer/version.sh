#!/bin/sh
#
# Copyright (c) 2016 The Voltage Project
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# set up the environment
get_os() {
    [ -x "/usr/bin/uname" ] && b_uname="/usr/bin/uname" || b_uname="/bin/uname"
    ${b_uname} -s
}

real_path() {
    local os=$(get_os)
    if [ "${os}" = "Darwin" ]; then
        [ -x "/usr/local/bin/perl" ] && b_perl="/usr/local/bin/perl" || b_perl="/usr/bin/perl"
        ${b_perl} -MCwd -le 'print Cwd::abs_path(shift)' "$0"
    else
        [ -x "/bin/realpath" ] && b_realpath="/bin/realpath" || b_realpath="/usr/bin/realpath"
        ${b_realpath} "$0"
    fi
}

SCRIPTPATH=$(real_path $0)
SCRIPTPREFIX=${SCRIPTPATH%/*}
. ${SCRIPTPREFIX}/common.sh

echo ${VERSION}

exit ${STATUS}
