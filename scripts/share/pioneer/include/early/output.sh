#!/bin/sh
#
# Copyright (c) 2016 The Voltage Project
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# include output colouring helpers
. ${SCRIPTPREFIX}/include/early/colour.sh

# enable color support
[ "${USE_COLOURS}"="yes" ] && colour_on

early_log() {
    [ "${SILENT}" = "yes" ] && return 0
    if [ -n "${COLOUR_ARROW}" ] || [ -z "${1##*\033[*}" ]; then
        printf "${COLOUR_ARROW}>>>${COLOUR_RESET} ${1}${COLOUR_RESET_REAL}\n"
    else
        printf ">>> ${1}\n"
    fi
}

early_log_error() {
    COLOUR_ARROW="${COLOUR_ERROR}${COLOUR_BOLD}" \
        early_log "${COLOUR_ERROR}${COLOUR_BOLD}Error:${COLOUR_RESET} $1" >&2
    return 0
}

early_log_warn() {
    COLOUR_ARROW="${COLOUR_WARN}${COLOUR_BOLD}" \
        early_log "${COLOUR_WARN}${COLOUR_BOLD}Warning:${COLOUR_RESET} $@" >&2
    return 0
}

early_log_debug() {
    [ ${DEBUG} = "yes" ] && COLOUR_ARROW="${COLOUR_DEBUG}${COLOUR_BOLD}" \
        early_log "${COLOUR_DEBUG}${COLOUR_BOLD}Debug:${COLOUR_RESET}${COLOUR_IGNORE} $@" >&2
    return 0
}

early_log_info() {
    COLOUR_ARROW="${COLOUR_INFO}${COLOUR_BOLD}" \
        early_log "${COLOUR_INFO}${COLOUR_BOLD}Info:${COLOUR_RESET}${COLOUR_IGNORE} $@" >&2
    return 0
}

early_die() {
    if [ $# -ne 2 ]; then
        early_die 1 "early_die() expects 2 arguments: exit_number \"message\""
    fi
    early_log_error "${2}" || :
    exit $1
}
