#!/bin/sh
#
# Copyright (c) 2016 The Voltage Project
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

colour_on() {
    if ! [ -t 1 ] || ! [ -t 2 ]; then
        USE_COLOURS="no"
    fi

    if [ ${USE_COLOURS} = "yes" ]; then
        COLOUR_RESET="\033[0;0m"
        COLOUR_RESET_REAL="${COLOUR_RESET}"
        COLOUR_BOLD="\033[1m"
        COLOUR_UNDER="\033[4m"
        COLOUR_BLINK="\033[5m"
        COLOUR_BLACK="\033[0;30m"
        COLOUR_RED="\033[0;31m"
        COLOUR_GREEN="\033[0;32m"
        COLOUR_AMBER="\033[0;33m"
        COLOUR_BLUE="\033[0;34m"
        COLOUR_MAGENTA="\033[0;35m"
        COLOUR_CYAN="\033[0;36m"
        COLOUR_LIGHT_GRAY="\033[0;37m"
        COLOUR_DARK_GRAY="\033[1;30m"
        COLOUR_LIGHT_RED="\033[1;31m"
        COLOUR_LIGHT_GREEN="\033[1;32m"
        COLOUR_YELLOW="\033[1;33m"
        COLOUR_LIGHT_BLUE="\033[1;34m"
        COLOUR_LIGHT_MAGENTA="\033[1;35m"
        COLOUR_LIGHT_CYAN="\033[1;36m"
        COLOUR_WHITE="\033[1;37m"

        COLOUR_WARN=${COLOUR_YELLOW}
        COLOUR_DEBUG=${COLOUR_CYAN}
        COLOUR_ERROR=${COLOUR_RED}
        COLOUR_INFO=${COLOUR_LIGHT_GRAY}
        COLOUR_NOTICE=${COLOUR_WHITE}
        COLOUR_SUCCESS=${COLOUR_GREEN}
        COLOUR_IGNORE=${COLOUR_DARK_GRAY}
        COLOUR_SKIP=${COLOUR_AMBER}
        COLOUR_FAIL=${COLOUR_RED}
        COLOUR_EM=${COLOUR_LIGHT_MAGENTA}${COLOUR_BOLD}
    fi
}

colour_off() {
    COLOUR_RESET=""
    COLOUR_RESET_REAL="${COLOUR_RESET}"
    COLOUR_BOLD=""
    COLOUR_UNDER=""
    COLOUR_BLINK=""
    COLOUR_BLACK=""
    COLOUR_RED=""
    COLOUR_GREEN=""
    COLOUR_AMBER=""
    COLOUR_BLUE=""
    COLOUR_CYAN=""
    COLOUR_MAGENTA=""
    COLOUR_LIGHT_GRAY=""
    COLOUR_DARK_GRAY=""
    COLOUR_LIGHT_RED=""
    COLOUR_LIGHT_GREEN=""
    COLOUR_YELLOW=""
    COLOUR_LIGHT_BLUE=""
    COLOUR_LIGHT_MAGENTA=""
    COLOUR_LIGHT_CYAN=""
    COLOUR_WHITE=""

    COLOUR_WARN=${COLOUR_YELLOW}
    COLOUR_DEBUG=${COLOUR_CYAN}
    COLOUR_ERROR=${COLOUR_RED}
    COLOUR_INFO=${COLOUR_LIGHT_GRAY}
    COLOUR_SUCCESS=${COLOUR_GREEN}
    COLOUR_IGNORE=${COLOUR_DARK_GRAY}
    COLOUR_SKIP=${COLOUR_AMBER}
    COLOUR_FAIL=${COLOUR_RED}
    COLOUR_EM=${COLOUR_LIGHT_MAGENTA}${COLOUR_BOLD}
}
