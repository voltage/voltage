#!/bin/sh
#
# Copyright (c) 2016 The Voltage Project
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# include binary detection
. ${SCRIPTPREFIX}/include/early/binary.sh

# include output colouring helpers
. ${SCRIPTPREFIX}/include/early/colour.sh

# include auxiliaries
. ${SCRIPTPREFIX}/include/normal/auxiliary.sh

# standard output is going to the terminal.
: ${OUTPUT_CHANNEL:=term}
[ "${USE_COLOURS}"="yes" ] && colour_on

_log_numeric_level() {
    # return the numeric value corresponding to a log level
    [ $# -ne 1 ] && return 1
    case ${1} in
        debug|DEBUG)
            echo 5
            ;;
        info|INFO)
            echo 4
            ;;
        notice|NOTICE)
            echo 3
            ;;
        warn|WARNING)
            echo 2
            ;;
        error|ERR)
            echo 1
            ;;
        silent|none)
            echo 0
            ;;
        *)
            echo 3
            ;;
    esac
    return 0
}

# log level remains as specified if no increment was indicated
: ${LOG_INCREMENT:=0}

# determine the real log level to be respected (numeric value)
LOG_LEVEL=$(_log_numeric_level ${LOG_LEVEL})
LOG_LEVEL=$((${LOG_LEVEL}+${LOG_INCREMENT}))
LOG_LEVEL=$(min ${LOG_LEVEL} 5)
LOG_LEVEL=$(max ${LOG_LEVEL} 0)

# module-specific variables
b_logger=$(detect_binary "logger")
b_date=$(detect_binary "date")

_log_term() {
    # Send log messages to a terminal
    #
    # Arguments:
    # (1) criticality - one of DEBUG, INFO, NOTICE, WARNING, ERR
    # (2) message     - the message to log

    # Verify two arguments were submitted
    [ $# -ne 2 ] && return 1

    # Respect the log level
    [ "$(_log_numeric_level ${1})" -gt "${LOG_LEVEL}" ] && return 0

    # Select message prefix
    local prefix=""
    case ${1} in
        DEBUG)
            prefix="${COLOUR_DEBUG}${COLOUR_BOLD}>>> Debg:${COLOUR_RESET}${COLOUR_IGNORE}"
            ;;
        INFO)
            prefix="${COLOUR_INFO}${COLOUR_BOLD}>>> Info:${COLOUR_RESET}${COLOUR_IGNORE}"
            ;;
        NOTICE)
            prefix="${COLOUR_NOTICE}${COLOUR_BOLD}>>> Note:${COLOUR_RESET}"
            ;;
        WARNING)
            prefix="${COLOUR_WARN}${COLOUR_BOLD}>>> Warn:${COLOUR_RESET}"
            ;;
        ERR)
            prefix="${COLOUR_ERROR}${COLOUR_BOLD}>>> Fail:${COLOUR_RESET}"
            ;;
        *)
            prefix="${COLOUR_NOTICE}${COLOUR_BOLD}>>> Note:${COLOUR_RESET}"
            ;;
    esac

    # output the log message
    printf "${prefix} ${2}${COLOUR_RESET_REAL}\n"
}

_log_file() {
    # Send log messages to a file
    #
    # Arguments:
    # (1) criticality - one of DEBUG, INFO, NOTICE, WARNING, ERR
    # (2) message     - the message to log

    # Verify two arguments were submitted
    [ $# -ne 2 ] && return 1

    # Respect the log level
    [ "$(_log_numeric_level ${1})" -gt "${LOG_LEVEL}" ] && return 0

    # Select message prefix
    local prefix=""
    local timestamp=$(${b_date} '+%Y-%m-%d %H:%M:%S')
    case ${1} in
        DEBUG)
            prefix="[${timestamp}] [DEBG]"
            ;;
        INFO)
            prefix="[${timestamp}] [INFO]"
            ;;
        NOTICE)
            prefix="[${timestamp}] [NOTE]"
            ;;
        WARNING)
            prefix="[${timestamp}] [WARN]"
            ;;
        ERR)
            prefix="[${timestamp}] [FAIL]"
            ;;
        *)
            prefix="[${timestamp}] [NOTE]"
            ;;
    esac

    # output the log message
    echo "${prefix} ${2}" >> "${LOG_FILE}"
}

_log_syslog() {
    # Send log messages to syslog
    #
    # Arguments:
    # (1) criticality - one of DEBUG, INFO, NOTICE, WARNING, ERR
    # (2) message     - the message to log

    # Verify two arguments were submitted
    [ $# -ne 2 ] && return 1

    # Respect the log level
    [ "$(_log_numeric_level ${1})" -gt "${LOG_LEVEL}" ] && return 0

    # Select message prefix
    local prefix=""
    local level=""
    case ${1} in
        DEBUG)
            prefix="[DEBG]"
            level="debug"
            ;;
        INFO)
            prefix="[INFO]"
            level="info"
            ;;
        NOTICE)
            prefix="[NOTE]"
            level="notice"
            ;;
        WARNING)
            prefix="[WARN]"
            level="warning"
            ;;
        ERR)
            prefix="[FAIL]"
            level="err"
            ;;
        *)
            prefix="[NOTE]"
            level="notice"
            ;;
    esac

    # output the log message
    ${b_logger} -t "${prefix}" -p "${LOG_TARGET}.${level}" "${2}"
}

log() {
    # log the submitted message to the currently valid logging channel
    #
    # Arguments:
    # (1) criticality - one of DEBUG, INFO, NOTICE, WARNING, ERR
    # (2) message     - the message to log

    # Verify two arguments were submitted
    [ $# -ne 2 ] && return 1

    # Respect the log level
    [ "$(_log_numeric_level ${1})" -gt "${LOG_LEVEL}" ] && return 0

    # log the message into the right channel
    case ${OUTPUT_CHANNEL} in
        term)
            _log_term ${1} "${2}"
            ;;
        file)
            _log_file ${1} "${2}"
            ;;
        syslog)
            _log_syslog ${1} "${2}"
            ;;
    esac
}

log_error() {
    # log the submitted message with error criticality

    # Verify an argument was submitted
    [ $# -ne 1 ] && return 1

    # Respect the log level
    [ "1" -gt "${LOG_LEVEL}" ] && return 0

    # log the message
    log "ERR" "${1}"
    return $?
}

log_warn() {
    # log the submitted message with warning criticality

    # Verify an argument was submitted
    [ $# -ne 1 ] && return 1

    # Respect the log level
    [ "2" -gt "${LOG_LEVEL}" ] && return 0

    # log the message
    log "WARNING" "${1}"
    return $?
}

log_note() {
    # log the submitted message with notice criticality

    # Verify an argument was submitted
    [ $# -ne 1 ] && return 1

    # Respect the log level
    [ "3" -gt "${LOG_LEVEL}" ] && return 0

    # log the message
    log "NOTICE" "${1}"
    return $?
}

log_info() {
    # log the submitted message with info criticality

    # Verify an argument was submitted
    [ $# -ne 1 ] && return 1

    # Respect the log level
    [ "4" -gt "${LOG_LEVEL}" ] && return 0

    # log the message
    log "INFO" "${1}"
    return $?
}

log_debug() {
    # log the submitted message with error criticality

    # Verify an argument was submitted
    [ $# -ne 1 ] && return 1

    # Respect the log level
    [ "5" -gt "${LOG_LEVEL}" ] && return 0

    # log the message
    log "DEBUG" "${1}"
    return $?
}

progress_msg() {
    [ "${OUTPUT_CHANNEL}" != "term" ] && return 0
    [ "3" -gt "${LOG_LEVEL}" ] && return 0
    COLOUR_ARROW="${COLOUR_INFO}${COLOUR_BOLD}"
    printf "${COLOUR_ARROW}>>>${COLOUR_RESET} ${COLOUR_INFO}${1}"
    printf "${COLOUR_BOLD} ... ${COLOUR_RESET_REAL}"
}

progress_success() {
    [ "${OUTPUT_CHANNEL}" != "term" ] && return 0
    [ "3" -gt "${LOG_LEVEL}" ] && return 0
    printf "${COLOUR_SUCCESS}${COLOUR_BOLD}success${COLOUR_RESET_REAL}\n"
}

progress_fail() {
    [ "${OUTPUT_CHANNEL}" != "term" ] && return 0
    [ "3" -gt "${LOG_LEVEL}" ] && return 0
    printf "${COLOUR_FAIL}${COLOUR_BOLD}fail${COLOUR_RESET_REAL}\n"
}

die() {
    if [ $# -ne 2 ]; then
        die 1 "die() expects 2 arguments: exit_number \"message\""
    fi
    log_error "${2}" || :
    exit $1
}
