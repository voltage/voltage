#!/bin/sh
#
# Copyright (c) 2016 The Voltage Project
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Pre-set information from calling binary
: ${STATUS:=0}
: ${USE_COLOURS:=yes}

# include non-configurable defaults
. ${SCRIPTPREFIX}/include/early/defaults.sh

# include early output handler
. ${SCRIPTPREFIX}/include/early/output.sh

# include binary detection
. ${SCRIPTPREFIX}/include/early/binary.sh

# include configurable defaults
. ${SCRIPTPREFIX}/include/early/config.sh

# include normal output handler
. ${SCRIPTPREFIX}/include/normal/output.sh

# include global auxiliary functions
. ${SCRIPTPREFIX}/include/normal/auxiliary.sh

# include file operation functions
. ${SCRIPTPREFIX}/include/normal/unistd.sh
