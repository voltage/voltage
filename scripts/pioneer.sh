#!/bin/sh
#
# Copyright (c) 2016 The Voltage Project
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# set up the environment
get_os() {
    [ -x "/usr/bin/uname" ] && b_uname="/usr/bin/uname" || b_uname="/bin/uname"
    ${b_uname} -s
}

real_path() {
    local os=$(get_os)
    if [ "${os}" = "Darwin" ]; then
        [ -x "/usr/local/bin/perl" ] && b_perl="/usr/local/bin/perl" || b_perl="/usr/bin/perl"
        ${b_perl} -MCwd -le 'print Cwd::abs_path(shift)' "$0"
    else
        [ -x "/bin/realpath" ] && b_realpath="/bin/realpath" || b_realpath="/usr/bin/realpath"
        ${b_realpath} "$0"
    fi
}

PIONEERPATH=$(real_path $0)
PIONEERPREFIX=${PIONEERPATH%\/pioneer*}
PIONEERPREFIX=${PIONEERPREFIX}/share/pioneer

# Pioneer version
PIONEER_VERSION="0.1-pre"

# undocumented flag for debugging purposes only
PIONEER_SETX=""

# increment or decrement for the log level
LOG_INCREMENT=0

while getopts "d:NSVvx" FLAG; do
    case ${FLAG} in
        d)
            LOG_INCREMENT=5
            ;;
        N)
            USE_COLOURS="no"
            ;;
        S)
            LOG_INCREMENT=-5
            ;;
        V)
            LOG_INCREMENT=$((${LOG_INCREMENT}-1))
            ;;
        v)
            LOG_INCREMENT=$((${LOG_INCREMENT}+1))
            ;;
        x)
            PIONEER_SETX="-x"
            ;;
        *)
            CMD=help
            STATUS=1
            ;;
    esac
done

shift $((OPTIND-1))
[ $# -lt 1 ] && CMD=help && STATUS=1

: ${CMD:=$1}
shift

case "${CMD}" in
    help|gitlab|version)
        ;;
    *)
        CMD=help
        STATUS=1
        ;;
esac

exec env -i PATH="$PATH" VERSION="$PIONEER_VERSION" STATUS=${STATUS} USE_COLOURS=${USE_COLOURS} \
    LOG_INCREMENT=${LOG_INCREMENT} /bin/sh ${PIONEER_SETX} "${PIONEERPREFIX}/${CMD}.sh" $@
