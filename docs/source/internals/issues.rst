Issue Management
================

Issues are managed centrally via `GitLab Issues`_. For better visualisation, the project uses the
integrated GitLab kanban board. Three types of labels are used to classify issues.


.. _issue-type:

Type
----

The type label indicates what kind the issue is of. These types are available:

* **bug** marks an error within the source code or the application logic
* **regression** marks an error that was introduced by another fix or feature
* **feature** marks a request for a new feature
* **enhancement** marks a request for enhancing an existing feature
* **optimisation** marks a request for optimising a feature or the corresponding source code
* **legal** marks issues related to legal affairs (e. g. licensing, patents, etc.)
* **support** marks issues being support requests


.. _issue-priority:

Priority
--------

The Voltage Project uses these priority labels:

* **Security** -- top priority label marking security issues
* **Critical** -- non-security related label for top priority issues endangering or blocking operation of the
  production system
* **High** -- high priority
* **Medium** -- medium priority
* **Low** -- low priority

Priorities may be subject to review by the :ref:`core team <core-team>`. They will adjust proposed priorities if
necessary to the right level.


.. _issue-status:

Status
------

The status labels are linked to the Kanban Board columns.

The Voltage Project uses these column labels (i. e. status messages):

* **Backlog** -- all issues that are waiting to be worked on.
* **In Progress** -- issues that are currently being worked on (i. e. a feature branch exists for these issues).
* **Ready** -- issues whose feature branch is considered ready for merge into the ``master`` branch (i. e. a merge
  request has been created for the respective issue).
* **Review or Blocked** -- issues which are subject to review by the :ref:`core team <core-team>`, pending validation
  or feedback or whose progress is blocked by whatever reason. Issues with this status are subject to regular reviews
  by the core team members.
* **Integration** -- issues whose feature branch has been merged into ``master`` and which can therefore be tested
  in the integration environment.
* **Done** -- issues that are considered complete after integration test. May be closed after review/confirmation.


.. rubric:: Footnotes

.. _GitLab Issues: https://gitlab.com/voltage/voltage/issues
