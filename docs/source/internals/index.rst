Project Documentation
=====================

This part of the documentation is for people hacking on Voltage itself. It is a mandatory
source of information for anyone wishing to contribute to Voltage's development. This guide
describes code standards as well as processes and tools around the management of Voltage's source code.

.. toctree::
   :maxdepth: 2

   code-style
   roles
   git
   issues
