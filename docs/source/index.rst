Voltage Documentation
=====================

This documentation is primarily a source for developers contributing to the Voltage project and administrators
maintaining it. However, this documentation does not cover general end user guidance.


How the documentation is organised
----------------------------------

* If you're a Voltage administrator, try the technical :doc:`Administrator's Guide <admin/index>` -- this
  might just be the documentation you're looking for.

* Looking for information on a specific application being part of Voltage? Check out the
  :doc:`Application Section <applications/index>`, providing information on all applications written for Voltage.

* The :doc:`Project Documentation <internals/index>` will tell you about the development process, and how you can
  contribute.

* :doc:`Reference guides <ref/index>` contain technical reference for APIs and other aspects of the Voltage
  machinery. They describe how it works and how to use it but assume that you have `Python`_ and `Django`_ knowledge.

.. _Python: https://www.python.org/
.. _Django: https://www.djangoproject.com/


Documentation Reference
-----------------------

Looking for a more structured overview? Have a look at our :doc:`detailed table of contents <contents>`, or -- when
seeking specific information -- the :ref:`genindex` and :ref:`modindex`. For terms and notions, check out the
:doc:`glossary`.
